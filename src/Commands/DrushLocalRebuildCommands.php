<?php

namespace Drupal\drush_locale_rebuild\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Database\Connection;

/**
 * Provides the locale:rebuild drush command.
 */
class DrushLocalRebuildCommands extends DrushCommands {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a DrushLocalRebuildCommands instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(
    StateInterface $state,
    Connection $database
  ) {
    $this->state = $state;
    $this->database = $database;
  }

  /**
   * Drush command to rebuild the locale (languages) from the language server.
   *
   * @command locale:rebuild
   * @usage locale:rebuild
   */
  public function rebuildLocale() {
    $this->database->query('DELETE FROM {key_value} WHERE collection=:collection', [':collection' => 'locale.translation_status']);
    $this->database->query('UPDATE {locale_file} SET timestamp = :zero', [':zero' => 0]);
    $this->database->query('UPDATE {locale_file} SET last_checked = :zero', [':zero' => 0]);
    $this->state->set('locale.translation_last_checked', 0);
    shell_exec('drush cr');
    shell_exec('drush locale-check && drush locale-update && drush cr');
  }

}
